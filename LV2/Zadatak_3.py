import matplotlib.pyplot as plt
import numpy as np
data = np.loadtxt(open("mtcars.csv", "rb"), usecols=(1,2,3,4,5,6), delimiter=",", skiprows=1)

mpg = data[:, 0]
hp = data[:,3]
wt = data[:,5]

print("Min mpg: ", np.min(mpg))
print("Max mpg: ", np.max(mpg))
print("Mean mpg: ", np.mean(mpg))

plt.scatter(wt, hp)
plt.xlabel('wt')
plt.ylabel('hp')
plt.show()



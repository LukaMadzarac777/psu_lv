import numpy as np
import matplotlib.pyplot as plt

x = [2, 3]
y = [2, 2]
x2 = [2, 1]
y2 = [2 ,1]
x3 = [1, 3]
y3 = [1 , 1]
x4 = [3,3]
y4=[2, 1]
plt.plot(x, y, '-', linewidth=1)
plt.plot(x2, y2, '-', linewidth=1)
plt.plot(x3, y3 , '-', linewidth=1)
plt.plot(x4, y4 , '-', linewidth=1)
plt.xlabel('x')
plt.ylabel('y')
plt.title('Primjer')
plt.show()